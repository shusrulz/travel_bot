
from typing import Dict, Text, Any, List, Union, Optional

from rasa_sdk import ActionExecutionRejection
from rasa_sdk import Action, Tracker
from rasa_sdk.events import SlotSet
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.forms import FormAction, REQUESTED_SLOT


class TravelForm(FormAction):
    """Example of a custom form action"""

    def name(self):
        # type: () -> Text
        """Unique identifier of the form"""

        return "travel_form"

    @staticmethod
    def required_slots(tracker: Tracker) -> List[Text]:
        """A list of required slots that the form has to fill"""

        return ["Location","Date_of_travel"]

    # def slot_mappings(self):
    #     # type: () -> Dict[Text: Union[Dict, List[Dict]]]
    #     """A dictionary to map required slots to
    #         - an extracted entity
    #         - intent: value pairs
    #         - a whole message
    #         or a list of them, where a first match will be picked"""

    #     return {"Location": [self.from_text()],"Date_of_travel":[self.from_text()]}

    def submit(self,
               dispatcher: CollectingDispatcher,
               tracker: Tracker,
               domain: Dict[Text, Any]) -> List[Dict]:
        """Define what the form has to do
            after all required slots are filled"""
        destination = tracker.get_slot('Location')
        date = tracker.get_slot('Date_of_travel')
        # api call(date,destination)
        msg =  'Booking flight for {}and{}'.format(destination,date)
        # utter submit template
        #dispatcher.utter_template('utter_slots_values', tracker)
        dispatcher.utter_message(msg)
        return []

class ActionShowFlightOptions(Action):
    def name(self):
        return 'actionshowflightoptions'
    def run(self,dispatcher,tracker,domain):
        Location = tracker.get_slot('Location')
        Date_of_travel = tracker.get_slot("Date_of_travel")
        response ="Which flight do you want to book?".format(Location,Date_of_travel)
        buttons = [{'title': 'Shree_Airlines',
                   'payload': '{}'.format('Shree_Airlines')},
                  {'title': 'Himalayan_flight',
                   'payload': '{}'.format('Himalayan_flight')}  
                   ]
        dispatcher.utter_message(response, buttons=buttons)

class ActionShowBookingOptions(Action):
    def name(self):
        return 'actionshowbookingoptions'
    def run(self, dispatcher, tracker,domain):
        message = "You can book flight following mediums::"
        buttons = [{'title': 'eSewa',
                   'payload': '{}'.format('eSewa')},
                  {'title': 'PayTM',
                   'payload': '{}'.format('PayTM')}  
                   ]
        dispatcher.utter_button_message(message, buttons=buttons)

class ActionBookTicket(Action):
	def name(self):
		return 'actionbookticket'

	def run(self,dispatcher,tracker,domain):
		booking_option = tracker.get_slot('booking_option')
		response = "Booking ticket through {}".format(booking_option)
		dispatcher.utter_message(response)
		return [SlotSet('booking_option', booking_option)]

