## intent:Goodbye
- Thanks
- Bye
- Goodbye
- Thanks for helping me out
- I am done.
- Thank you for guiding me
- Yes, thank you for helping me out.
- Good bye
- Bye
- Thank You
- Byebye
- Goodbye
- Thanks for helping me out
- Thank you
- Thanks
- Thanks for helping
- Thank you for helping me out
- thank you

## intent:Greeting
- Hi
- Hello
- what's up?
- How you doin?
- Good morning
- Good evening
- Hey
- Hello, how are you?
- Hello chatbot
- Hello travelbot
- hi
- hello
- HI
- Hello
- Hey, what is up?
- Hey, what' up
- Hey. what's up?

## intent:about_travel_bot
- What is this chatbot?
- What can travel bot do?
- What is travel bot?
- What can you do?
- What services do you provide?
- What are you capable of?
- What is rasa chatbot?
- How good is this chatbot?
- How can you help me?

## intent:flight_available
- [Shree_Airlines](flight_options:shree_airlines)
- [Shree_Airlines](flight_options)
- [Shree_Airlines](flight_options)
- [Himalayan_flight](flight_options:himalayan_flight)
- [Shree_Airlines](flight_options)
- [Himalayan_flight](flight_options)
- [Shree_Airlines](flight_options)
- [Himalayan_flight](flight_options)
- [Shree_Airlines](flight_options)
- [Himalayan_flight](flight_options)
- [Shree_Airlines](flight_options)
- [Himalayan_flight](flight_options)
- [Shree_Airlines](flight_options)
- [Himalayan_flight](flight_options)
- [Shree_Airlines](flight_options)
- [Himalayan_flight](flight_options)
- [Himalayan_flight](flight_options)
- [Shree_Airlines](flight_options)
- [Himalayan_flight](flight_options)
- [eSewa](booking_option:esewa)
- [PayTM](booking_option:paytm)

## intent:out_of_scope
- This is not a good service
- This is not good chatbot.
- Nonsense
- What the heck is this?
- I don't want to use this chatbot.
- I don't want to travel.
- Shut up.

## intent:travel_packages
- [Kathmandu Tour for 10 days](travel_packages_options:kathmandu tour for 10 days)
- [Pokhara Tour for 3 nights 4 days](travel_packages_options:pokhara tour for 3 nights 4 days)
- [Kathmandu Tour for 10 days](travel_packages_options)
- [Kathmandu Tour for 10 days](travel_packages_options)
- [Pokhara Tour for 3 nights 4 days](travel_packages_options)
- /travel_packages[{"travel_packages_options":"Pokhara Tour for 3 nights 4 days"}](travel_packages_options:Pokhara Tour for 3 nights 4 days)

## intent:travel_plan
- I want to travel to [Italy](Location) in [2018-1-1](Date_of_travel) through [PayTM](booking_option)
- Show me the holiday packages for [Rome](Location) in [2017-8-9](Date_of_travel).
- What are the available travel packages.
- I want to travel.
- Do you have any packages for [Europe](Location) trip in [2015-6-5](Date_of_travel)?
- Show me the available offers.
- Are there any offers for [Europe](Location) trip in [2018-9-4](Date_of_travel)?
- Show me offers for [Chitiwan](Location).
- Give me the list of destinations for [Europe](Location) trip.
- What are the details for [Africa](Location) trip?
- Is there any deals for [Australia](Location).
- What offers do you have right Now?
- I want to go on a vacation.
- I want a holiday destination in [USA](Location).
- What travel offers do you have Now?
- Provide me the list of your tour packages.
- Provide me with the available holiday plans.
- Show me your offers.
- What offers do you have for the [Pokhara](Location) trip?
- I want to travel to [Italy](Location)
- I want to travel
- [Nepal](Location:nepal)
- [India](Location)
- [China](Location)
- [USA](Location)
- [England](Location)
- [Spain](Location)
- [Argentina](Location)
- [2015-6-5](Date_of_travel:2015 - 6 - 5)
- [2018-4-7](Date_of_travel)
- [2015-5-5](Date_of_travel)
- [2013-5-1](Date_of_travel)
- [Norway](Location)
- I want to go on a vacation
- I want to go on vacation
- [2016-7-8](Date_of_travel)
- go on a vacation
- [2013-9-5](Date_of_travel)
- Thailand
- [2019-8-9](Date_of_travel)
- [2016-8-5](Date_of_travel:2016 - 8 - 5)
- i want to travel
- I want to go to vacation
- [2015-6-7](Date_of_travel)
- i want to go on vacation
- [Italy](Location)
- [2015-6-9](Date_of_travel)
- [2015-8-8](Date_of_travel)
- [2015-9-9](Date_of_travel)
- [2015-9-3](Date_of_travel)
- Book me a ticket for [Italy](Location) on [2015-4-6](Date_of_travel) through [eSewa](booking_option).
- Book me a ticket for [Italy](Location) on [2015-4-6](Date_of_travel) through [PayTM](booking_option).
- [2014-6-1](Date_of_travel)
- [2015-4-2](Date_of_travel)
- [2015-6-17](Date_of_travel)
- [2015-8-4](Date_of_travel)
- [2015-3-1](Date_of_travel)
- [2015-6-3](Date_of_travel)
- [2014-6-3](Date_of_travel)
- [2015-6-4](Date_of_travel)
- I want to travel
- [Nepal](Location)
- [2015-8-5](Date_of_travel)
- [eSewa](booking_option)
- Guide me for travelling.
- [Nepal](Location)
- [2015-3-7](Date_of_travel)
- [eSewa](booking_option)
- [Switzerland](Location:switzerland)
- [2015-3-5](Date_of_travel)
- [2015-3-4](Date_of_travel)
- [2015-8-7](Date_of_travel)
- Guide me to travel
- Guide me through travelling
- [2015-8-3](Date_of_travel)
- Travel me through travelling
- [Australia](Location)
- I want to chill out
- [2019-8-7](Date_of_travel:2019 - 8 - 7)
- I want to book a flight for [Italy](Location)
- [2019-8-4](Date_of_travel)
- I want to book a flight for [2019/4/7](Date_of_travel)
- I want to book a flight for [2019-7-4](Date_of_travel)
- I want o book a flight to Colombia
- I want to book a flight for [Colombia](Location)
- [2019-5-7](Date_of_travel)
- [eSewa](booking_option)
- Book a flight to [Netharlands](Location)
- [2019-8-3](Date_of_travel)
- [PayTM](booking_option)
- I want to book a flight to [America](Location) for [2019-7-3](Date_of_travel)
- [PayTM](booking_option)
- Book a flight to [Switzerland](Location)
- [2019-7-5](Date_of_travel)
- [eSewa](booking_option)
- I want to book a flight to [Argentina](Location) for the date of [2015-8-4](Date_of_travel) through [eSewa](booking_option)
- [1995-6-8](Date_of_travel:1995 - 6 - 8)
- [2015-9-8](Date_of_travel:2015 - 9 - 8)
- [2015-6-8](Date_of_travel:2015 - 6 - 8)

## synonym:1995 - 6 - 8
- 1995-6-8

## synonym:2015 - 6 - 5
- 2015-6-5

## synonym:2015 - 9 - 8
- 2015-9-8

## synonym:2016 - 8 - 5
- 2016-8-5

## synonym:Pokhara Tour for 3 nights 4 days
- {"travel_packages_options":"Pokhara Tour for 3 nights 4 days"}

## synonym:esewa
- eSewa

## synonym:himalayan_flight
- Himalayan_flight

## synonym:kathmandu tour for 10 days
- Kathmandu Tour for 10 days

## synonym:nepal
- Nepal

## synonym:paytm
- PayTM

## synonym:pokhara tour for 3 nights 4 days
- Pokhara Tour for 3 nights 4 days

## synonym:shree_airlines
- Shree_Airlines
