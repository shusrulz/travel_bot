##Story1
* Greeting
    - utter_greet

##GoodbyeStory
* Goodbye
    - utter_goodbye

##AboutStory
* about_travel_bot
    -utter_about_travel_bot

##Out_of_scope_story
* out_of_scope
    - utter_out_of_scope

## New Story

* Greeting
    - utter_greet
* travel_plan
    - travel_form
    - slot{"requested_slot":"Location"}
* travel_plan{"Location":"nepal"}
    - travel_form
    - slot{"Location":"nepal"}
    - slot{"requested_slot":"Date_of_travel"}
* travel_plan{"Date_of_travel":"2015 - 6 - 5"}
    - travel_form
    - slot{"Date_of_travel":"2015 - 6 - 5"}
    - slot{"requested_slot":null}
    - form{"name": null}
    - action_deactivate_form
    - slot{"requested_slot":null}
    - form{"name": null}
    - utter_travel_packages
* travel_packages{"travel_packages_options":"kathmandu tour for 10 days"}
    - slot{"travel_packages_options":"kathmandu tour for 10 days"}
    - actionshowflightoptions
* flight_available{"flight_options":"himalayan_flight"}
    - slot{"flight_options":"himalayan_flight"}
    - actionshowbookingoptions
* flight_available{"booking_option":"paytm"}
    - slot{"booking_option":"paytm"}
    - actionbookticket
    - slot{"booking_option":"paytm"}
    - utter_booking_successfull
* Goodbye
    - utter_goodbye
    - action_restart

## New Story

* Greeting
    - utter_greet
* travel_plan
    - travel_form
    - slot{"requested_slot":"Location"}
* travel_plan{"Location":"USA"}
    - travel_form
    - slot{"Location":"USA"}
    - slot{"requested_slot":"Date_of_travel"}
* travel_plan{"Date_of_travel":"2016-7-5"}
    - travel_form
    - slot{"Date_of_travel":"2016-7-5"}
    - slot{"requested_slot":null}
    - form{"name": null}
    - action_deactivate_form
    - slot{"requested_slot":null}
    - form{"name": null}
    - utter_travel_packages
* travel_packages{"travel_packages_options":"kathmandu tour for 10 days"}
    - slot{"travel_packages_options":"kathmandu tour for 10 days"}
    - actionshowflightoptions
* flight_available{"flight_options":"himalayan_flight"}
    - slot{"flight_options":"himalayan_flight"}
    - actionshowbookingoptions
* flight_available{"booking_option":"eSewa"}
    - slot{"booking_option":"eSewa"}
    - actionbookticket
    - slot{"booking_option":"eSewa"}
    - utter_booking_successfull
* Goodbye
    - utter_goodbye
    - action_restart

##Story1
* Greeting
    - utter_greet

##GoodbyeStory
* Goodbye
    - utter_goodbye

##AboutStory
* about_travel_bot
    -utter_about_travel_bot

##Out_of_scope_story
* out_of_scope
    - utter_out_of_scope

##Story1
* Greeting
    - utter_greet
* travel_plan
    - travel_form
    - slot{"requested_slot":"Location"}
* travel_plan{"Location":"Nepal"}
    - travel_form
    - slot{"Location":"Nepal"}
    - slot{"requested_slot":"Date_of_travel"}
* travel_plan{"Date_of_travel":"2014-6-3"}
    - travel_form
    - slot{"Date_of_travel":"2014-6-3"}
    - slot{"requested_slot":null}
    - form{"name": null}
    - action_deactivate_form
    - slot{"requested_slot":null}
    - form{"name": null}
    - utter_travel_packages
    - slot{"travel_packages_options":"Kathmandu Tour for 10 days"}
    - actionshowflightoptions
* flight_available{"flight_options":"Shree_Airlines"}
    - slot{"flight_options":"Shree_Airlines"}
    - actionshowbookingoptions
* travel_plan{"booking_option":"PayTM"}
    - slot{"booking_option":"PayTM"}
    - actionbookticket
    - slot{"booking_option":"PayTM"}
    - utter_booking_successfull
    - action_restart

## Story2
* Greeting
    - utter_greet
* travel_plan
    - travel_form
    - slot{"requested_slot":"Location"}
* travel_plan{"Location":"Italy"}
    - travel_form
    - slot{"Location":"Italy"}
    - slot{"requested_slot":"Date_of_travel"}
* travel_plan{"Date_of_travel":"2015-6-4"}
    - travel_form
    - slot{"Date_of_travel":"2015-6-4"}
    - slot{"requested_slot":null}
    - form{"name": null}
    - action_deactivate_form
    - slot{"requested_slot":null}
    - form{"name": null}
    - utter_travel_packages
* travel_packages{"travel_packages_options":"kathmandu tour for 10 days"}
    - slot{"travel_packages_options":"kathmandu tour for 10 days"}
    - actionshowflightoptions
* flight_available{"flight_options":"Shree_Airlines"}
    - slot{"flight_options":"Shree_Airlines"}
    - actionshowbookingoptions
* travel_plan{"booking_option":"PayTM"}
    - slot{"booking_option":"PayTM"}
    - actionbookticket
    - slot{"booking_option":"PayTM"}
    - utter_booking_successfull
    - action_restart

## interactive_story_1
* Greeting
    - utter_greet
* travel_plan
    - travel_form
    - form{"name": "travel_form"}
    - slot{"requested_slot": "Location"}
* form: travel_plan{"Location": "Nepal"}
    - form: travel_form
    - slot{"Location": "Nepal"}
    - slot{"requested_slot": "Date_of_travel"}
* form: travel_plan{"Date_of_travel": "2015-8-5"}
    - travel_form
    - slot{"Date_of_travel": "2015-8-5"}
    - slot{"requested_slot":null}
    - form{"name": null}
    - action_deactivate_form
    - slot{"requested_slot":null}
    - form{"name": null}
    - utter_travel_packages
* travel_packages{"travel_packages_options":"kathmandu tour for 10 days"}
    - slot{"travel_packages_options":"kathmandu tour for 10 days"}
    - actionshowflightoptions
* flight_available{"flight_options":"Shree_Airlines"}
    - slot{"flight_options":"Shree_Airlines"}
    - actionshowbookingoptions
* travel_plan{"booking_option": "eSewa"}
    - slot{"booking_option": "eSewa"}
    - actionbookticket
    - slot{"booking_option": "eSewa"}
    - utter_booking_successfull
    - action_restart

## interactive_story_1
* Greeting
    - utter_greet
* travel_plan
    - travel_form
    - form{"name": "travel_form"}
    - slot{"requested_slot": "Location"}
* form: travel_plan{"Location": "Italy"}
    - form: travel_form
    - slot{"Location": "Italy"}
    - slot{"requested_slot": "Date_of_travel"}
* form: travel_plan{"Date_of_travel": "2016-8-5"}
    - form: travel_form
    - slot{"Date_of_travel": "2016-8-5"}
    - slot{"requested_slot":null}
    - form{"name": null}
    - action_deactivate_form
    - slot{"requested_slot":null}
    - form{"name": null}
    - utter_travel_packages
* travel_packages{"travel_packages_options":"kathmandu tour for 10 days"}
    - slot{"travel_packages_options":"kathmandu tour for 10 days"}
    - actionshowflightoptions
* flight_available{"flight_options":"Shree_Airlines"}
    - slot{"flight_options":"Shree_Airlines"}
    - actionshowbookingoptions
* travel_plan{"booking_option": "PayTM"}
    - slot{"booking_option": "PayTm"}
    - actionbookticket
    - slot{"booking_option": "PayTM"}
    - utter_booking_successfull
    - action_restart

## interactive_story_1
* Greeting
    - utter_greet
* travel_plan
    - travel_form
    - form{"name": "travel_form"}
    - slot{"requested_slot": "Location"}
* form: travel_plan{"Location": "Nepal"}
    - form: travel_form
    - slot{"Location": "Nepal"}
    - slot{"requested_slot": "Date_of_travel"}
* form: travel_plan{"Date_of_travel": "2015-3-7"}
    - form: travel_form
    - slot{"Date_of_travel": "2015-3-7"}
    - slot{"requested_slot":null}
    - form{"name": null}
    - action_deactivate_form
    - slot{"requested_slot":null}
    - form{"name": null}
    - utter_travel_packages
* travel_packages{"travel_packages_options":"kathmandu tour for 10 days"}
    - slot{"travel_packages_options":"kathmandu tour for 10 days"}
    - actionshowflightoptions
* flight_available{"flight_options":"Shree_Airlines"}
    - slot{"flight_options":"Shree_Airlines"}
    - actionshowbookingoptions
* travel_plan{"booking_option": "eSewa"}
    - slot{"booking_option": "eSewa"}
    - actionbookticket
    - slot{"booking_option": "eSewa"}
    - utter_booking_successfull
    - action_restart

## New Story

* Greeting
    - utter_greet
* travel_plan
    - travel_form
    - slot{"requested_slot":"Location"}
* travel_plan{"Location":"Nepal"}
    - travel_form
    - slot{"Location":"Nepal"}
    - slot{"requested_slot":"Date_of_travel"}
* travel_plan{"Date_of_travel":"2015-3-7"}
    - travel_form
    - slot{"Date_of_travel":"2015-3-7"}
    - slot{"requested_slot":null}
    - form{"name": null}
    - action_deactivate_form
    - slot{"requested_slot":null}
    - form{"name": null}
    - utter_travel_packages
* travel_packages{"travel_packages_options":"kathmandu tour for 10 days"}
    - slot{"travel_packages_options":"kathmandu tour for 10 days"}
    - actionshowflightoptions
* flight_available{"flight_options":"Shree_Airlines"}
    - slot{"flight_options":"Shree_Airlines"}
    - actionshowbookingoptions
* travel_plan{"booking_option":"eSewa"}
    - slot{"booking_option":"eSewa"}
    - actionbookticket
    - slot{"booking_option":"eSewa"}
    - utter_booking_successfull
    - action_restart

## New Story

* Greeting
    - utter_greet
* travel_plan
    - travel_form
    - slot{"requested_slot":"Location"}
* travel_plan{"Location":"Nepal"}
    - travel_form
    - slot{"Location":"Nepal"}
    - slot{"requested_slot":"Date_of_travel"}
* travel_plan{"Date_of_travel":"2015-3-5"}
    - travel_form
    - slot{"Date_of_travel":"2015-3-5"}
    - slot{"requested_slot":null}
    - form{"name": null}
    - action_deactivate_form
    - slot{"requested_slot":null}
    - form{"name": null}
    - utter_travel_packages
* travel_packages{"travel_packages_options":"kathmandu tour for 10 days"}
    - slot{"travel_packages_options":"kathmandu tour for 10 days"}
    - actionshowflightoptions
* flight_available{"flight_options":"Shree_Airlines"}
    - slot{"flight_options":"Shree_Airlines"}
    - actionshowbookingoptions
* travel_plan{"booking_option":"PayTM"}
    - slot{"booking_option":"PayTM"}
    - actionbookticket
    - slot{"booking_option":"PayTM"}
    - utter_booking_successfull
    - action_restart

## New Story

* Greeting
    - utter_greet
* travel_plan
    - travel_form
    - slot{"requested_slot":"Location"}
* travel_plan{"Location":"Nepal"}
    - travel_form
    - slot{"Location":"Nepal"}
    - slot{"requested_slot":"Date_of_travel"}
* travel_plan{"Date_of_travel":"2015-6-7"}
    - travel_form
    - slot{"Date_of_travel":"2015-6-7"}
    - slot{"requested_slot":null}
    - form{"name": null}
    - action_deactivate_form
    - slot{"requested_slot":null}
    - form{"name": null}
    - utter_travel_packages
* travel_packages{"travel_packages_options":"kathmandu tour for 10 days"}
    - slot{"travel_packages_options":"kathmandu tour for 10 days"}
    - actionshowflightoptions
* flight_available{"flight_options":"Himalayan_flight"}
    - slot{"flight_options":"Himalayan_flight"}
    - actionshowbookingoptions
* travel_plan{"booking_option":"PayTM"}
    - slot{"booking_option":"PayTM"}
    - actionbookticket
    - slot{"booking_option":"PayTM"}
    - utter_booking_successfull
    - action_restart

## New Story

* Greeting
    - utter_greet
* travel_plan
    - travel_form
    - slot{"requested_slot":"Location"}
* travel_plan{"Location":"Nepal"}
    - travel_form
    - slot{"Location":"Nepal"}
    - slot{"requested_slot":"Date_of_travel"}
* travel_plan{"Date_of_travel":"2015-6-7"}
    - travel_form
    - slot{"Date_of_travel":"2015-6-7"}
    - slot{"requested_slot":null}
    - travel_form
    - slot{"Location":"Nepal"}
    - slot{"Date_of_travel":"2015-6-7"}
    - slot{"requested_slot":null}
    - form{"name": null}
    - action_deactivate_form
    - slot{"requested_slot":null}
    - form{"name": null}
    - utter_travel_packages
* travel_packages{"travel_packages_options":"kathmandu tour for 10 days"}
    - slot{"travel_packages_options":"kathmandu tour for 10 days"}
    - actionshowflightoptions
* flight_available{"flight_options":"Himalayan_flight"}
    - slot{"flight_options":"Himalayan_flight"}
    - actionshowbookingoptions
* travel_plan{"booking_option":"eSewa"}
    - slot{"booking_option":"eSewa"}
    - actionbookticket
    - slot{"booking_option":"eSewa"}
    - utter_booking_successfull
    - action_restart

## New Story

* Greeting
    - utter_greet
* travel_plan
    - travel_form
    - slot{"requested_slot":"Location"}
* travel_plan{"Location":"Australia"}
    - travel_form
    - slot{"Location":"Australia"}
    - slot{"requested_slot":"Date_of_travel"}
* travel_plan{"Date_of_travel":"2015-6-7"}
    - travel_form
    - slot{"Date_of_travel":"2015-6-7"}
    - slot{"requested_slot":null}
    - travel_form
    - slot{"Location":"Nepal"}
    - slot{"Date_of_travel":"2015-6-7"}
    - slot{"requested_slot":null}
    - form{"name": null}
    - action_deactivate_form
    - slot{"requested_slot":null}
    - form{"name": null}
    - utter_travel_packages
* travel_packages{"travel_packages_options":"kathmandu tour for 10 days"}
    - slot{"travel_packages_options":"kathmandu tour for 10 days"}

    - actionshowflightoptions
* flight_available{"flight_options":"Himalayan_flight"}
    - slot{"flight_options":"Himalayan_flight"}
    - actionshowbookingoptions
* travel_plan{"booking_option":"PayTM"}
    - slot{"booking_option":"PayTM"}
    - actionbookticket
    - slot{"booking_option":"PayTM"}
    - utter_booking_successfull
    - action_restart

## New Story

* Greeting
    - utter_greet
* travel_plan
    - travel_form
    - slot{"requested_slot":"Location"}
* travel_plan{"Location":"Australia"}
    - travel_form
    - slot{"Location":"Australia"}
    - slot{"requested_slot":"Date_of_travel"}
* travel_plan{"Date_of_travel":"2011-6-7"}
    - travel_form
    - slot{"Date_of_travel":"2011-6-7"}
    - slot{"requested_slot":null}
    - travel_form
    - slot{"Location":"Nepal"}
    - slot{"Date_of_travel":"2011-6-7"}
    - slot{"requested_slot":null}
    - form{"name": null}
    - action_deactivate_form
    - slot{"requested_slot":null}
    - form{"name": null}
    - utter_travel_packages
* travel_packages{"travel_packages_options":"kathmandu tour for 10 days"}
    - slot{"travel_packages_options":"kathmandu tour for 10 days"}
    - actionshowflightoptions
* flight_available{"flight_options":"Himalayan_flight"}
    - slot{"flight_options":"Himalayan_flight"}
    - actionshowbookingoptions
* travel_plan{"booking_option":"PayTM"}
    - slot{"booking_option":"PayTM"}
    - actionbookticket
    - slot{"booking_option":"PayTM"}
    - utter_booking_successfull
    - action_restart

## New Story

* Greeting
    - utter_greet
* travel_plan
    - travel_form
    - slot{"requested_slot":"Location"}
* travel_plan{"Location":"Nepal"}
    - travel_form
    - slot{"Location":"Nepal"}
    - slot{"requested_slot":"Date_of_travel"}
* travel_plan{"Date_of_travel":"2019-8-7"}
    - travel_form
    - slot{"Date_of_travel":"2019-8-7"}
    - slot{"requested_slot":null}
    - form{"name": null}
    - action_deactivate_form
    - slot{"requested_slot":null}
    - form{"name": null}
    - utter_travel_packages
* travel_packages{"travel_packages_options":"kathmandu tour for 10 days"}
    - slot{"travel_packages_options":"kathmandu tour for 10 days"}
    - actionshowflightoptions
* flight_available{"flight_options":"Himalayan_flight"}
    - slot{"flight_options":"Himalayan_flight"}
    - actionshowbookingoptions
* travel_plan{"booking_option":"eSewa"}
    - slot{"booking_option":"eSewa"}
    - actionbookticket
    - slot{"booking_option":"eSewa"}
    - utter_booking_successfull

## New Story

* Greeting
    - utter_greet

## New Story

* Greeting
    - utter_greet
* travel_plan{"Location":"Italy"}
    - travel_form
    - slot{"Location":"Italy"}
    - slot{"requested_slot":"Date_of_travel"}
* travel_plan{"Date_of_travel":"2019-8-4"}
    - travel_form
    - slot{"Date_of_travel":"2019-8-4"}
    - slot{"requested_slot":null}
    - form{"name": null}
    - action_deactivate_form
    - slot{"requested_slot":null}
    - form{"name": null}
    - utter_travel_packages
* travel_packages{"travel_packages_options":"kathmandu tour for 10 days"}
    - slot{"travel_packages_options":"kathmandu tour for 10 days"}
    - actionshowflightoptions
* flight_available{"flight_options":"Himalayan_flight"}
    - slot{"flight_options":"Himalayan_flight"}
    - actionshowbookingoptions
* travel_plan{"booking_option":"PayTM"}
    - slot{"booking_option":"PayTM"}
    - actionbookticket
    - slot{"booking_option":"PayTM"}
    - utter_booking_successfull
    - action_restart

## New Story
* travel_plan{"Date_of_travel":"2019-7-4"}
    - travel_form
    - slot{"Date_of_travel":"2019-7-4"}
    - slot{"requested_slot":"Location"}
* form: travel_plan{"Location": "Nepal"}
    - form: travel_form
    - slot{"Location": "Nepal"}
    - slot{"requested_slot":null}
    - form{"name": null}
    - action_deactivate_form
    - slot{"requested_slot":null}
    - form{"name": null}
    - utter_travel_packages
* travel_packages{"travel_packages_options":"kathmandu tour for 10 days"}
    - slot{"travel_packages_options":"kathmandu tour for 10 days"}
    - actionshowflightoptions
* flight_available{"flight_options":"Himalayan_flight"}
    - slot{"flight_options":"Himalayan_flight"}
    - actionshowbookingoptions
* travel_plan{"booking_option":"PayTM"}
    - slot{"booking_option":"PayTM"}
    - actionbookticket
    - slot{"booking_option":"PayTM"}
    - utter_booking_successfull
    - action_restart

## interactive_story_1
* travel_plan{"Location": "Colombia"}
    - travel_form
    - form{"name": "travel_form"}
    - slot{"Location": "Colombia"}
    - slot{"requested_slot": "Date_of_travel"}
* form: travel_plan{"Date_of_travel": "2019-5-7"}
    - form: travel_form
    - slot{"Date_of_travel": "2019-5-7"}
    - slot{"requested_slot":null}
    - form{"name": null}
    - action_deactivate_form
    - slot{"requested_slot":null}
    - form{"name": null}
    - utter_travel_packages
* travel_packages{"travel_packages_options":"kathmandu tour for 10 days"}
    - slot{"travel_packages_options":"kathmandu tour for 10 days"}
    - actionshowflightoptions
* flight_available{"flight_options": "Shree_Airlines"}
    - slot{"flight_options": "Shree_Airlines"}
    - actionshowbookingoptions
* travel_plan{"booking_option": "eSewa"}
    - slot{"booking_option": "eSewa"}
    - actionbookticket
    - slot{"booking_option": "eSewa"}
    - utter_booking_successfull
    - action_restart

## interactive_story_1
* travel_plan{"Location": "Netharlands"}
    - travel_form
    - form{"name": "travel_form"}
    - slot{"Location": "Netharlands"}
    - slot{"requested_slot": "Date_of_travel"}
* form: travel_plan{"Date_of_travel": "2019-8-3"}
    - form: travel_form
    - slot{"Date_of_travel": "2019-8-3"}
    - slot{"requested_slot":null}
    - form{"name": null}
    - action_deactivate_form
    - slot{"requested_slot":null}
    - form{"name": null}
    - utter_travel_packages
* travel_packages{"travel_packages_options":"kathmandu tour for 10 days"}
    - slot{"travel_packages_options":"kathmandu tour for 10 days"}
    - actionshowflightoptions
* flight_available{"flight_options": "Himalayan_flight"}
    - slot{"flight_options": "Himalayan_flight"}
    - actionshowbookingoptions
* travel_plan{"booking_option": "PayTM"}
    - slot{"booking_option": "PayTM"}
    - actionbookticket
    - slot{"booking_option": "PayTM"}
    - utter_booking_successfull
* Goodbye
    - utter_goodbye
    - action_restart

## interactive_story_1
* travel_plan{"Location": "America", "Date_of_travel": "2019-7-3"}
    - travel_form
    - form{"name": "travel_form"}
    - slot{"Location": "America"}
    - slot{"Date_of_travel": "2019-7-3"}
    - slot{"requested_slot":null}
    - form{"name": null}
    - action_deactivate_form
    - slot{"requested_slot":null}
    - form{"name": null}
    - utter_travel_packages
* travel_packages{"travel_packages_options":"kathmandu tour for 10 days"}
    - slot{"travel_packages_options":"kathmandu tour for 10 days"}
    - actionshowflightoptions
* flight_available{"flight_options": "Himalayan_flight"}
    - slot{"flight_options": "Himalayan_flight"}
    - actionshowbookingoptions
* travel_plan{"booking_option": "PayTM"}
    - slot{"booking_option": "PayTM"}
    - actionbookticket
    - slot{"booking_option": "PayTM"}
    - utter_booking_successfull
* Goodbye
    - utter_goodbye
    - action_restart

## interactive_story_1
* travel_plan{"Location": "Argentina", "Date_of_travel": "2015-8-4", "booking_option": "eSewa"}
    - slot{"booking_option": "eSewa"}
    - utter_travel_packages
* travel_packages{"travel_packages_options":"kathmandu tour for 10 days"}
    - slot{"travel_packages_options":"kathmandu tour for 10 days"}
    - actionshowflightoptions
* flight_available{"flight_options": "Himalayan_flight"}
    - slot{"flight_options": "Himalayan_flight"}
    - actionbookticket
    - slot{"booking_option": "eSewa"}
    - utter_booking_successfull
* Goodbye
    - utter_goodbye
    - action_restart

## interactive_story_1
* travel_plan{"Location": "Argentina", "Date_of_travel": "2015-8-4", "booking_option": "PayTM"}
    - slot{"booking_option": "PayTM"}
    - utter_travel_packages
* travel_packages{"travel_packages_options":"kathmandu tour for 10 days"}
    - slot{"travel_packages_options":"kathmandu tour for 10 days"}
    - actionshowflightoptions
* flight_available{"flight_options": "Himalayan_flight"}
    - slot{"flight_options": "Himalayan_flight"}
    - actionbookticket
    - slot{"booking_option": "eSewa"}
    - utter_booking_successfull
* Goodbye
    - utter_goodbye 
    - action_restart

## New Story

* Greeting
    - utter_greet
* travel_plan
    - travel_form
    - slot{"requested_slot":"Location"}
* travel_plan{"Location":"nepal"}
    - travel_form
    - slot{"Location":"nepal"}
    - slot{"requested_slot":"Date_of_travel"}
* travel_plan{"Date_of_travel":"2016 - 8 - 5"}
    - travel_form
    - slot{"Date_of_travel":"2016 - 8 - 5"}
    - slot{"requested_slot":null}
    - form{"name": null}
    - action_deactivate_form
    - slot{"requested_slot":null}
    - form{"name": null}
    - utter_travel_packages
* travel_packages{"travel_packages_options":"kathmandu tour for 10 days"}
    - slot{"travel_packages_options":"kathmandu tour for 10 days"}
    - actionshowflightoptions
* flight_available{"flight_options":"shree_airlines"}
    - slot{"flight_options":"shree_airlines"}
    - actionshowbookingoptions
* flight_available{"booking_option":"esewa"}
    - slot{"booking_option":"esewa"}
    - actionbookticket
    - slot{"booking_option":"esewa"}
    - utter_booking_successfull
* Goodbye
    - utter_goodbye
    - action_restart
